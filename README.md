# spacetime-plots

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/millernb%2Flinear-grapher/master?filepath=spacetime_plotter.ipynb)

A python noteboook for plotting points and lines, expressly written for making [spacetime diagrams](https://en.wikipedia.org/wiki/Spacetime_diagram). To get started with a tutorial, [launch the binder instance of the notebook](https://mybinder.org/v2/gl/millernb%2Flinear-grapher/master?filepath=spacetime_plotter.ipynb).

## Purpose

The purpose of this repo is to abstract away the details of creating a plot in `matplotlib`, a module which is notoriously tricky for first-time users (especially those with little or no programming experience). For instance, [per the documentation](https://matplotlib.org/3.1.1/gallery/lines_bars_and_markers/simple_plot.html#sphx-glr-gallery-lines-bars-and-markers-simple-plot-py), here is about the simplest plot one can make using `matplotlib`. 

```python
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
t = np.arange(0.0, 2.0, 0.01)
s = 1 + np.sin(2 * np.pi * t)

fig, ax = plt.subplots()
ax.plot(t, s)

ax.set(xlabel='time (s)', ylabel='voltage (mV)',
       title='About as simple as it gets, folks')
ax.grid()

plt.show()
```

![image](./figs/matplotlib_simple.png)

 This "simple example", however, is already too complicated for our purposes -- we're only interested in plotting lines and points. To that end, let's plot a couple line. Recall that a line can be specified by either (1) a pair of points or (2) a point and a line. Suppose we're interested in plotting two lines with slope $`-0.5`$, one  passing through  $`A = (1, 2)`$ and the other passing through $`B = (1, -1)`$. Using `matplotlib`, we cannot directly plot this line -- instead, we must convert this description of a line into a collection of points, which can then be plotted via `matplotlib.pyplt.plot`. 
 
 Using either the code in this repo or `matplotlib`, we should get the following plot.
 
 ![image](figs/two_lines.png)
 
Here is the matplotlib implementation.

 ```python
import matplotlib 
import matplotlib.pyplot as plt
import numpy as np

# Determine range
xlim = (-5, 5)

# Line a
m_a = -0.5
A = (1, 2)
label_a = 'a'

# Line b
m_b = -0.5
B = (1, -1)
label_b = 'b'

# Convert description to arrays
for label, m, P in zip([label_a, label_b], [m_a, m_b], [A, B]):
    b = P[1] - m *P[0]
    x = np.linspace(xlim[0], xlim[1])
    y = m *x + b

    # Actually plot the line
    plt.plot(x, y, label=label, lw=2)

# Format plot to look nice
plt.xlim(xlim)
plt.ylim(xlim)
plt.gca().set_aspect('equal')
plt.minorticks_on()
plt.tick_params(direction='in', which='both')
plt.grid(which='major', alpha=0.7)
plt.grid(which='minor', alpha=0.2)

plt.legend(bbox_to_anchor=(1, 1), loc='upper left', prop={'size': 16})

plt.axhline(0, ls='--')
plt.axvline(0, ls='--')

plt.grid()
plt.show()
 ```

Compare with the much simpler implementation in this repo.
```python
from plotter import plot_diagram

# Change these
xlim = (-5, 5)
lines = [
    ('a', (1,  2), -0.5), # line a
    ('b', (1, -1), -0.5), # line b
]

# Make plot
plot_diagram(lines, xlim)
```

Of course, there are additional wrinkles to a `matplotlib` approach that must be accounted for (plotting vertical lines, specifying a line by two points instead, plotting a single point). The `matplotlib` wrapper `plot_diagram` takes care of those issues for us.


## How to use

The notebook (`spacetime_plotter.ipynb`) is just a frontend for the function `plot_diagram` inside `plotter.py`. That function takes two mandatory arguments: 

1. `arr`: an array of tuples, which specify the points and lines, and
2. `xlim`: a tuple determining the limits on the graph. 

To see what this means, consider the following examples. 

### Tom's worldline

Suppose we want to draw a spacetime diagram for Tom in his own reference frame. In his own reference frame $`\beta \equiv v/c =  0`$, so Tom's wordline is vertical worldline and passes though the origin. We can make such a spacetime diagram by running

```python
xlim = (-5, 5)
spacetime_objects = [
    ('Tom', (0, 0), inf), # this is a worldline
]

plot_diagram(spacetime_objects, xlim)
```

which would produce the following plot

![image](./figs/tom_wl.png)


Notice `xlim` determines the bounds for the $`x`$-axis and $`y`$-axis. This isn't done automatically -- it must be specified!

Recall that a line is determined by either (1) two different points or (2) a slope and a point. In this case we have specified Tom's worldline by a point and a slope:

```python
('Tom', (0, 0), inf)
```

The first entry in the tuple (`Tom`) determines the label for the legend. The second entry in the tuple is itself another tuple, which in this case represents a point the line passes through (the origin `(0, 0)`). The last entry in the tuple denotes the slope (`inf`). 

Notice `inf` is shorthand for infinity (`np.inf`). Tom's wordline is vertical, so the slope is indeterminate.
```math
\lim_{\Delta x \rightarrow 0} \frac{\Delta y}{\Delta x} \rightarrow \infty \qquad \text{(or sometimes $-\infty$)}
```

We have specified Tom's worldline using a point and slope. We could also use two different points to determine the worldline. All of the following are equivalent:

```python
('Tom', (0, 0), inf)    # a worldline         (label, point, slope)
('Tom', inf, (0, 0))    # the same worldline  (label, slope, point)
('Tom', (0, 0), (0, 1)) # also the worldline  (label, point, point)
```

### Adding a second worldline

Suppose we wish to add Sarah's worldline. Sarah is moving with $`v = -0.5c`$ and collides with Tom at $`ct = 6`$. Thus the slope of her worldline is $`1/\beta = -2`$ and the event "Sarah collides with Tom" occurs at the point $`(0, 6)`$. We add Sarah's worldline with the following addition (notice that I've changed `xlim`).

```python
xlim = (-1, 7)
spacetime_objects = [
    ('Tom', (0, 0), inf),   # Tom's worldline
    ('Sarah', (0, 6), -2),  # Sarah's worldline
]

plot_diagram(spacetime_objects, xlim)
```

![image](./figs/sarah_wl.png)

Notice the comma after the tuple `('Tom', (0, 0), inf)`. Without it, the program will fail with `TypeError: 'tuple' object is not callable`!

### Adding an event
Recall that an _event_ in special relativity is equivalent to a point on a spacetime diagram. Suppose we wish to add the event "Sarah's position at $`t = 0`$". Per the diagram, this event occurs at $`(3, 0)`$, which we'll label as event $`B`$. In this program, an event is specified by `(label, point)`. Thus we would add

```python
xlim = (-1, 7)
spacetime_objects = [
    ('Tom', (0, 0), inf),   # Tom's worldline
    ('Sarah', (0, 6), -2),  # Sarah's worldline
    ('B', (3, 0)),          # Sarah's position at t=0
]

plot_diagram(spacetime_objects, xlim)
```

![image](./figs/event_b.png)


### Extra options

Final there are some additional options which aren't necessary but may be useful. The labels for the $`x`$-axis and $`y`$-axis can be specified by `xlabel` and `ylabel`, respectively. LaTeX formatting is permited.  Similarly, titles can be specified by `title`.

If we specify a worldline/event with label `''` or `None`, we get a dull line/point which doesn't appear in the legend. This can be useful if we want to draw a ray of light without cluttering the plot.

Finally, the labels can be placed directly on the plot (instead of the legend) by specifying `annotate=True`. However, this isn't guaranteed to work in all cases, in which case one might have to fiddle with `xlim`.  

Combining all these options, we might get something like this.

```python
xlim = (-1, 7)
spacetime_objects = [
    ('Tom', (0, 0), inf),   # Tom's worldline
    ('Sarah', (0, 6), -2),  # Sarah's worldline

    ('B', (3, 0)),          # Sarah's position at t=0

    ('', (3, 0), -1)        # A ray of light
]
xlabel = '$x$'
ylabel = '$ct$'
title = 'Exp 1: Tom throws a ball'

plot_diagram(spacetime_objects, xlim, xlabel=xlabel, ylabel=ylabel, title=title, annotate=True)
```

![image](./figs/extra_options.png)