import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from itertools import cycle
matplotlib.rcParams['figure.figsize'] = [10, 10]

inf = np.inf

def plot_diagram(arr, xlim, xlabel=None, ylabel=None, title=None, annotate=False):
    """
    Plots lines and points.

    Args:
        arr: an array of tuples, taking one of the following forms:
            1. (label, point, point)
            2. (label, point, slope)
            3. (label, slope, point)
            4. (label, point)
        Here 'point' is itself a tuple. The first three tuples specify a line; the last specifies a point

        xlim: The x- and y-limits of the axis

        xlabel: the xlabel

        ylabel: the ylabel

        title: the title 

        annotate: show labels directly on plot without legend (sometimes buggy)

    Returns: a plot
    """
    ylim = xlim

    # for iterating through color cycles
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = cycle(prop_cycle.by_key()['color'])

    # convert array of tuples into something that can be plotted
    plot_objects = {}
    for idx, tup in enumerate(arr):
        # index
        plot_objects[idx] = {}

        # get label
        plot_objects[idx]['label'] = tup[0]

        # handle points
        if len(tup) == 2 and hasattr(tup[1], '__len__'):
            plot_objects[idx]['x'] = tup[1][0]
            plot_objects[idx]['y'] = tup[1][1]

        # handle lines
        elif len(tup) == 3:

            # get slope  
            # tup = (label, slope, point)
            if not hasattr(tup[1], '__len__'):
                slope = tup[1]
            # tup = (label, point, slope)
            elif not hasattr(tup[2], '__len__'):
                slope = tup[2]
            # tup = (label, point, point)
            elif tup[2][0] == tup[1][0]:
                slope = np.inf
            # tup = (label, point, point)
            else:
                slope = (tup[2][1] - tup[1][1]) / (tup[2][0] - tup[1][0])

            # Calculate (x1, y1) and (x2, y2), which will be used for making a line
            if slope == np.inf: 
                x = np.repeat([tup[j][0] for j in [1, 2] if hasattr(tup[j], '__len__')][0], 2)
                y = np.array(ylim)
            # timelike
            elif np.abs(slope) > 1:    
                b = [tup[j][1] - slope *tup[j][0] for j in [1, 2] if hasattr(tup[j], '__len__')][0]
                y = (np.array(ylim))
                x = (np.array(ylim) - b) / slope
            # spacelike/null
            else:
                b = [tup[j][1] - slope *tup[j][0] for j in [1, 2] if hasattr(tup[j], '__len__')][0]
                x = np.array(xlim)
                y = slope *x + b
                
            plot_objects[idx]['x'] = x
            plot_objects[idx]['y'] = y
            plot_objects[idx]['slope'] = slope

        else:
            raise ValueError(tup, 'defines neither a point nor line.')

    # plot points, lines
    dy_text = -10
    for idx in plot_objects:
        label = plot_objects[idx]['label'] 
        if label == '':
            label = None

        if label is None:
            color = 'grey'
            alpha = 0.25
        else:
            alpha = 1.0
            color = next(colors) # iterate through color cycle

        if not hasattr(plot_objects[idx]['y'], '__len__'):
            plt.scatter(plot_objects[idx]['x'] , plot_objects[idx]['y'], s=100, label=label, color=color, ec='k', alpha=alpha, zorder=10)
            if annotate:
                plt.annotate(label, (plot_objects[idx]['x'] , plot_objects[idx]['y']), fontsize='x-large', weight='bold', zorder=11, xytext=(0, 10), textcoords='offset pixels')
        else: # other lines
            if annotate and np.abs(plot_objects[idx]['slope']) > 1:
                dy_text -= 10
                plt.annotate(label, (plot_objects[idx]['x'][1], plot_objects[idx]['y'][1]), fontsize='x-large', weight='bold', zorder=11, xytext=(0, dy_text), textcoords='offset pixels', ha='center')
            elif annotate:
                plt.annotate(label, (plot_objects[idx]['x'][1], plot_objects[idx]['y'][1]), fontsize='x-large', weight='bold', zorder=11, xytext=(0, 10), textcoords='offset pixels', ha='center')
            plt.plot(plot_objects[idx]['x'] , plot_objects[idx]['y'], lw=2, label=label, color=color, alpha=alpha)
            

    # Format plot axis etc
    if not annotate:
        plt.legend(bbox_to_anchor=(1, 1), loc='upper left', prop={'size': 16})

    plt.axhline(0, ls='--')
    plt.axvline(0, ls='--')

    plt.xlabel(xlabel, fontsize=24)
    plt.ylabel(ylabel, fontsize=24)
    plt.title(title, fontsize=24)

    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.gca().set_aspect('equal')

    plt.minorticks_on()
    plt.tick_params(direction='in', which='both')
    plt.grid(which='major', alpha=0.7)
    plt.grid(which='minor', alpha=0.2)

    fig = plt.gcf()
    #plt.close()
    plt.draw()
    return None

    return fig